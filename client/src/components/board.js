import React, { Component } from 'react';
import { listenForCursorChange, listenForNewWords } from '../api';

class Board extends Component {
    constructor(props) {
        super(props);

        listenForCursorChange((err, positions) => {
            this.setState({
                cursorPosition:
                {
                    left: positions[this.props.playerId].cursorPosition,
                    right: positions[this.props.playerId === 1 ? 2 : 1].cursorPosition
                }
            });
        });
        listenForNewWords((err, words) => {
            if (this.props.playerId === 2) {
                this.setState({ words: this.flipBoard(words) });
            }
            else {
                this.setState({ words });
            }
        });
    }

    componentDidMount() {
        this.setState({
            cursorPosition: {
                left: this.props.initialCursorPositions[1],
                right: this.props.initialCursorPositions[2]
            }
        });
    }

    state = {
        cursorPosition: { left: 3, right: 2 },
        dimensions: { column: this.props.columns, row: this.props.rows },
        words: null
    };

    renderCellContent(row, column) {
        const rightMostColumn = this.state.dimensions.column - 1;
        const cursorPosition = this.state.cursorPosition;
        const words = this.state.words;
        const redLetterStyle = { color: 'red' };

        if (column === 0 && cursorPosition.left === row) return '>';
        else if (column === rightMostColumn && row === cursorPosition.right) return '<';
        else if (column === 0 || column === rightMostColumn) return '';
        else if (words) {
            const word = words.find((element) => {
                return element.word[row + '-' + column];
            });
            if (word) {
                if (word.word[row + '-' + column].matched) {
                    return <span style={redLetterStyle}>{word.word[row + '-' + column].letter}</span>;
                }
                else {
                    return <span>{word.word[row + '-' + column].letter}</span>;
                }
            }
        }
        return '';
    }

    renderRow(rowNo) {
        var cells = [];
        for (var i = 0; i < this.props.columns; i++) {
            cells.push(
                <td className="typing-row" key={`cell-${rowNo}-${i}`}>
                    {this.renderCellContent(rowNo, i)}
                </td>);
        }
        return cells;
    }

    renderBoard() {
        var rows = [];
        for (var i = 0; i < this.props.rows; i++) {
            rows.push(<tr key={`row-${i}`}>{this.renderRow(i)}</tr>);
        }
        return rows;
    }

    flipBoard(words) {
        function flip(word, boardColumns) {
            var nameless = [];
            var renumbered = [];
            var flipped = {}

            for (var key in word) {
                nameless.push(word[key]);
            }

            //nameless = nameless.sort((a, b) => {
            //    return parseInt(a.key.split('-')[1], 10) - parseInt(b.key.split('-')[1], 10);
            //});

            nameless.forEach((element) => {
                const column = parseInt(element.key.split('-')[1], 10);
                renumbered.push(boardColumns - column - 1);
            });

            renumbered.sort((a, b) => { return a - b; });

            for (var i = 0; i < nameless.length; i++) {
                const originalRow = nameless[i].key.split('-')[0];
                const newKey = originalRow + '-' + renumbered[i];
                nameless[i].key = newKey;
                flipped[newKey] = nameless[i];
            }

            return flipped;
        }
        var flipped = words;
        flipped = flipped.map((value) => {
            value.word = flip(value.word, this.state.dimensions.column);

            return value;
        });
        return flipped;
    }

    render() {
        return (
            <div>
                <table className="typing-board">
                    <tbody>
                        {this.renderBoard()}
                    </tbody>
                </table>
            </div>
        );
    };
}

export default Board;