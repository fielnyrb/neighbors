import openSocket from 'socket.io-client';
const socket = openSocket('http://localhost:8000');

export const DOWN = 'down';
export const UP = 'up';

function subscribeToServer(callback) {
    socket.on('initialize', initials => callback(null, initials));
    socket.emit('subscribeToTimer', 1000);
}
function listenForCursorChange(callback) {
    socket.on('cursor_position', position => callback(null, position));
}
function listenForNewWords(callback) {
    socket.on('words_to_client', words => callback(null, words));
}
function listenForCharacter(callback) {
    socket.on('char_to_client', character => callback(null, character));
}
function listenForPointUpdates(callback) {
    socket.on('points_update', points => callback(null, points));
}
function moveCursor(direction, playerNo) {
    socket.emit('cursor', { direction: direction, playerNumber: playerNo });
}
function transmitWord(word, playerNo) {
    socket.emit('word_to_server', { word: word, playerNumber: playerNo });
}
function transmitCharacter(character, playerNo) {
    socket.emit('char_to_server', { character, playerNo });
}

export {
    subscribeToServer,
    listenForCursorChange,
    moveCursor,
    transmitWord,
    transmitCharacter,
    listenForNewWords,
    listenForCharacter,
    listenForPointUpdates,
};