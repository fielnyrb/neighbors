import React, { Component } from 'react';
import './App.css';
import {
    subscribeToServer,
    moveCursor,
    transmitWord,
    transmitCharacter,
    listenForPointUpdates,
    listenForCharacter,
    UP,
    DOWN
} from './api';
import Board from './components/board';

class App extends Component {
    state = {
        playerId: null,
        dimensions: {rows: 10, columns: 30},
        initialCursorPositions: { 1: 5, 2: 5 },
        currentWord: '',
        points: { 1: 0, 2: 0 }
    };

    componentDidMount() {
        subscribeToServer((err, initialSettings) => {
            if (!this.state.playerId) {
                this.setState({
                    playerId: initialSettings.playerId,
                    dimensions: initialSettings.tableDimensions,
                    points: initialSettings.points
                });
            }
        });
        listenForPointUpdates((err, points) => {
            this.setState({ points: points.points });
        });
        listenForCharacter((err, character) => {
            if (character.playerNo === this.state.playerId) {
                this.setState({ currentWord: this.state.currentWord + character.character });
            }
        });
    }

    handleKeyDown(e) {
        if (e.keyCode === 40) {
            moveCursor(DOWN, this.state.playerId);
        }
        else if (e.keyCode === 38) {
            moveCursor(UP, this.state.playerId);
        }
        else if (e.keyCode === 13) {
            transmitWord(this.state.currentWord, this.state.playerId);
            this.setState({ currentWord: '' });
        }
        else {
            if (e.keyCode >= 48 && e.keyCode <= 90) {
                const character = String.fromCharCode(e.keyCode);
                transmitCharacter(character, this.state.playerId);
            }
        }
    }

    render() {
        return (
            <div className="App" onKeyDown={this.handleKeyDown.bind(this)} tabIndex="0">
                <p className="App-intro">
                    Player: {this.state.playerId}
                </p>
                <p>Word: {this.state.currentWord}</p>
                <Board columns={this.state.dimensions.columns}
                    rows={this.state.dimensions.rows}
                    playerId={this.state.playerId}
                    initialCursorPositions={this.state.initialCursorPositions} />
                <div>
                    <h3>Points</h3>
                    <p>Player 1: {this.state.points[1]}, Player 2: {this.state.points[2]}</p>
                </div>
            </div>
        );
    }
}

export default App;
