const GameUtils = require('./gameUtils');

const INITIAL_CURSOR_POSITIONS = { 1: 5, 2: 5 };
const TABLE_ROWS = 10;
const TABLE_COLUMNS = 30;
const UP = 'up';
const DOWN = 'down';

class Game {
    constructor() {
        this.activePlayers = 0;
        this.players = {
            1: {
                cursorPosition: INITIAL_CURSOR_POSITIONS[1],
                points: 0
            },
            2: {
                cursorPosition: INITIAL_CURSOR_POSITIONS[2],
                points: 0
            }
        };
        this.words = [];
        this.gameUtils = new GameUtils();
        this.ignoreNextCharacterReturn = false;
    }

    getStartData() {
        return {
            playerId: this.activePlayers,
            tableDimensions: { rows: TABLE_ROWS, columns: TABLE_COLUMNS },
            initialCursorPositions: INITIAL_CURSOR_POSITIONS,
            points: { 1: this.players[1].points, 2: this.players[2].points }
        }
    }

    moveCursor(player, direction) {
        const upOrDown = direction === UP ? -1 : 1;

        const newPosition = this.players[player].cursorPosition + upOrDown;

        if (newPosition >= 10 || newPosition < 0) {
            return this.players;
        }

        this.players[player].cursorPosition = newPosition;
        return this.players;
    }

    receiveWord(player, word) {
        const position = this.determineWordPosition(player, word);

        this.words.push({
            player: player,
            word: this.gameUtils.adaptWord(word, position.row, position.column)
        });

        return this.words;
    }

    receiveCharacter(player, character) {
        const oppositePlayer = this.gameUtils.getOppositePlayer(player);
        const wordOnSameRowIndex = this.words.findIndex((word) => {
            var firstKey = null;
            var row = null;

            firstKey = Object.keys(word.word)[0];
            try {
                row = word.word[firstKey].key.split('-')[0];
            }
            catch (err) {
                return;
            }

            if (parseInt(this.players[player].cursorPosition, 10) === parseInt(row, 10)) {
                return true;
            }
            return false;
        });

        const wordOnSameRow = this.words[wordOnSameRowIndex];

        if (wordOnSameRow) {
            this.ignoreNextCharacterReturn = true;
            if (wordOnSameRow.player === oppositePlayer) {
                var leadingLetter = wordOnSameRow.word[Object.keys(wordOnSameRow.word)[0]];
                var unMatchedExists = false;

                for (var key in wordOnSameRow.word) {
                    if (wordOnSameRow.word[key].letter === character && !wordOnSameRow.word[key].matched) {
                        wordOnSameRow.word[key].matched = true;
                        break;
                    }
                }
                for (var nKey in wordOnSameRow.word) {
                    if (!wordOnSameRow.word[nKey].matched) {
                        unMatchedExists = true;
                    }
                }
                if (!unMatchedExists) {
                    this.addPointTo(player);
                    this.words = this.words.filter((value, index) => {
                        if (index === wordOnSameRowIndex) {
                            return false;
                        }
                        return true;
                    });
                }
            }
        }
    }

    determineWordPosition(player, word) {
        const row = player === 1 ? this.players[1].cursorPosition : this.players[2].cursorPosition;
        const column = player === 1 ? 0 : TABLE_COLUMNS - word.length - 2;

        return { row, column }
    }

    moveWords() {
        if (this.words) {
            this.words = this.words.map((value) => {
                const direction = value.player === 1 ? 1 : -1;
                var movedWord = {};
                movedWord.player = value.player;
                movedWord.word = {};

                for (var key in value.word) {
                    const oldPosition = value.word[key].key.split('-');
                    const newKey = oldPosition[0] + '-' + (parseInt(oldPosition[1], 10) + direction);
                    movedWord.word[newKey] = value.word[key];
                    movedWord.word[newKey].key = newKey;
                }
                return movedWord;
            });
        }
        return this.words;
    }

    isOffTheBoard(word) {
        var letterInsideLimitFound = false;
        for (var key in word) {
            const columnKey = parseInt(key.split('-')[1], 10);
            if (columnKey < TABLE_COLUMNS &&
                columnKey > 0 &&
                !Number.isNaN(columnKey)) {
                letterInsideLimitFound = true;
            }
        }
        return !letterInsideLimitFound;
    }

    cleanDepartedWords() {
        var self = this;
        this.words = this.words.filter((element) => {
            if (this.isOffTheBoard(element.word)) {
                this.addPointTo(element.player);
                return false;
            }
            return true;
        });
        return this.words;
    }

    addPointTo(player) {
        this.players[player].points = this.players[player].points + 1;
    }

    connectNewPlayer() {
        if (this.playerSpotIsAvailable()) {
            this.activePlayers++;
            return true;
        }
        return false;
    }

    playerSpotIsAvailable() {
        if (this.activePlayers < 2) {
            return true;
        }
        return false;
    }

}

module.exports = {
    Game: Game,
    INITIAL_CURSOR_POSITIONS: INITIAL_CURSOR_POSITIONS,
    TABLE_ROWS: TABLE_ROWS,
    TABLE_COLUMNS: TABLE_COLUMNS
};