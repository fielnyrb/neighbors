const io = require('socket.io')();
const game = require('./game');

class ConnectionManager {
    constructor() {
        this.startNewGame();
    }

    startNewGame() {
        this.gg = new game.Game();
    }

    setUpChannels() {
        io.on('connection', (client) => {
            if (this.gg.connectNewPlayer() === true) {
                console.log('A user connected.');
                console.log('Number of players: ' + this.gg.activePlayers);
                client.emit('initialize', this.gg.getStartData());
            }
            else {
                console.log('Lobby is full (2 players). Restart game to add players.');
            }

            client.on('cursor', (data) => {
                const players = this.gg.moveCursor(data.playerNumber, data.direction);
                io.emit('cursor_position', players);
            });

            client.on('word_to_server', (data) => {
                const words = this.gg.receiveWord(data.playerNumber, data.word);
                io.emit('words_to_client', words);
            });
            client.on('char_to_server', (data) => {
                this.gg.receiveCharacter(data.playerNo, data.character);
                if (!this.gg.ignoreNextCharacterReturn) {
                    io.emit('char_to_client', data);
                }
                else {
                    this.gg.ignoreNextCharacterReturn = false;
                    io.emit('words_to_client', this.gg.words);
                }
            });
        });
    }
    startListening() {
        setInterval(() => {
            this.gg.moveWords();
            const words = this.gg.cleanDepartedWords();
            io.emit('words_to_client', words);
            io.emit('points_update', { points: { 1: this.gg.players[1].points, 2: this.gg.players[2].points } });
        }, 250);


        const port = 8000;
        io.listen(port);
        console.log('Listening on port', port);
    }
}

module.exports = ConnectionManager;