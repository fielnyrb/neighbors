var _ = require('lodash')

class GameUtils {
    adaptWord(word, row, column = 0) {
        var brokenIntoChars = word.split('').map((val, index) => {
            return { letter: val, key: row + '-' + (index+column+1) };
        });
        return _.keyBy(brokenIntoChars,'key');
    }
    getOppositePlayer(player) {
        return player === 1 ? 2 : 1;
    }
}

module.exports = GameUtils;